
Versions
===========
0.1 - initial


Running Containers Instructions
===============================

docker run --name=magento2 -p 10002:80 --restart=always -d -v /c/Users/Adrian/www/magento2:/var/www/html adighe/magento2

docker run --name=magento2 -p 10002:80 --restart=always -d -v /Users/adrian.gheorghe/Projects/Magento2/code:/var/www/html adighe/magento2


Building Image (Only if you want to change it)
==============
docker build -t adighe/magento2 .


Todos
===============

- Add DB container
- Add memcached
- Add Redis
- Add Varnish




